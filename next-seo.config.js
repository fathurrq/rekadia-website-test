// eslint-disable-next-line import/no-anonymous-default-export
export default {
  openGraph: {
    type: 'website',
    locale: 'en_US',
    url: 'https://rekadia.co.id/',
    site_name: 'PT Rekadia Solusi Teknologi'
  },
  description: 'PT Rekadia Solusi Teknologi'
}
