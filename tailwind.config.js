const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './component/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontSize: {
      'xs': '.75rem',
      'sm': '.875rem',
      'tiny': '.875rem',
      'base': '1rem',
      'lg': '1.125rem',
      'xl': '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem',
      'header': '2.63rem'
    },
    fontFamily: {
      'roboto': 'Roboto',
      slab : ['Roboto-Slab']
    },
    screens: {
      'sm': '320px',
      // => @media (min-width: 640px) { ... }

      'md': '500px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }
    },
    extend: {
      colors: {
        highlight: '#df3751',
        active: '#4ACFFF',
        'active-lighter': '#42C1EE',
        'not-active': '#888888',
        primary: '#3DA6D0',
        dark: '#4A4A4A',
        'light-grey': '#9B9B9B',
        'lighter-grey': '#949494',
        'line-grey': '#f2f2f2',
        'medium-grey': '#E3E3E3',
        darker: '#1F2320',
        'dark-blue': '#113C6D',
        'darker-blue': '#317C9E',
        'light-green': '#49C2BA',
        'darkest-blue': '#113C6D',
        'mobile-navbar-dark': '#12306E',
        'mobile-navbar-blue': '#13508D'
      },
      spacing: {
        sm: '8px',
        md: '16px',
        lg: '24px',
        xl: '48px',
        header: '618px',
        '30': '7.5rem',
        '110': '28rem',
        '120': '28.67rem',
        '74': '18.75rem',
        '150': '37.50rem',
        '51': '12.50rem',
        '530': '33rem',
        '534': '33.25rem',
        '571': '35.75rem',
        '472': '29.50rem',
        '440': '27.5rem',
        '500': '31.25rem',
        '876': '54.75rem',
        '900' : '56.25rem',
        '400': '25rem',
        '15': '3.75rem',
        '84': '21rem',
        '85': '21.25rem',
        '21': '5.25rem',
        '25': '6.25rem',
        '275': '17.25rem',
        'mobile-menu': '21.5rem'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
