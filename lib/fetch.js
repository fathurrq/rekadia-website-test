import { orderItems } from "./order"

//For single content fetching
export const fetchSingleContent = async (endpoint) =>{
    const data = await fetch(endpoint)
    const [output] = await data.json()
    return output
}

//For whole menu fetching
export const fetchMenuContent = async (url, endpoint) => {
    const json_data = await fetch(url + endpoint)
    const data = await json_data.json()
    orderItems(data)
    return data
}