export const orderItems = (data) => {
    data.sort((a, b) => {
        return a.acf.order - b.acf.order;
    });
    
}