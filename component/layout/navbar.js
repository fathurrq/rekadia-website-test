import Link from 'next/link'
import { useState } from 'react'
import { useRouter } from 'next/router'

const menu = [
    {
        name: "About Us",
        key: 1,
        link: "/about-us",
    },
    {
        name: "Solution",
        key: 2,
        link: "solution",
        subMenu: [{
            name: 'Production Monitoring Software',
            key: 1,
            link: '/solution/product-monitoring'
        },
        {
            name: 'Maintenance & Inspection Software',
            key: 2,
            link: '/solution/maintenance'
        },
        {
            name: 'HSSE Software',
            key: 3,
            link: '/solution/hsse'
        },
        {
            name: 'Other',
            key: 4,
            link: '/solution/other'
        }
        ]
    },
    {
        name: "Contact Us",
        key: 3,
        link: "/contact-us",
    },
    {
        name: "Join With Us",
        key: 4,
        link: "/join-us",
    },
]
const Navbar = () => {
    const router = useRouter()
    const [dropDown, setDropDown] = useState(false)
    const [mobileDropDown, setMobileDropDown] = useState(false)
    const [expand, setExpand] = useState(false)
    return (
        <div className="xl:absolute lg:absolute sm:sticky top-0 flex justify-between w-full items-end xl:border-b lg:border-b sm:border-b-0 border-gray-400 sm:bg-gradient-to-b sm:from-mobile-navbar-dark sm:to-mobile-navbar-blue xl:from-transparent xl:to-transparent lg:from-transparent lg:to-transparent xl:bg-transparent lg:bg-transparent xl:h-30 lg:h-30 sm:h-21 z-50">
            <div className="xl:p-8 lg:p-8 sm:p-4 xl:pb-4 lg:pb-4 flex cursor-pointer">
            <Link href={'/'} passHref>
                    <img src={'/logo.png'} className='sm:w-24 sm:h-7 xl:w-auto lg:w-auto xl:h-auto lg:h-auto' alt='logo' />
                </Link>
            </div>
            <div className="sm:flex xl:hidden lg:hidden pr-4 pb-4" onClick={() => setMobileDropDown(!mobileDropDown)}>
                <img alt='dropdown' src={mobileDropDown ? '/exit.png' : '/menu.png'} className="cursor-pointer"/>
            </div>
            <div className=" pb-3 sm:hidden xl:flex lg:flex">
                {
                    menu.map((d, i) => {
                        if (d.subMenu) {
                            return <div className="relative m-0 p-0" key={i}>
                                <a className={router.pathname.split('/')[1] === d.link ? "mx-4 text-active-lighter font-bold lg:text-sm xl:text-lg" : "mx-4  text-medium-grey lg:text-sm xl:text-lg font-roboto-medium"} onClick={() => {
                                    setDropDown(!dropDown)
                                }}>{d.name}</a>
                                {
                                    dropDown ?
                                        <div className="origin-top-right absolute left-0 top-9 mt-2 w-64 shadow-lg bg-white ring-1 ring-black ring-opacity-5 divide-y divide-light-grey divide- focus:outline-none background-nav">
                                            {
                                                d.subMenu.map((d, i) => (
                                                    <div key={i}>
                                                        <Link href={d.link} passHref>
                                                            <a href="#" className={router.pathname === d.link ? "text-active-lighter block px-4 py-1.5 text-sm font-bold" : "text-light-grey block px-4 py-1.5 text-sm font-roboto-medium"}>{d.name}</a>
                                                        </Link>
                                                    </div>
                                                ))
                                            }
                                        </div> : null
                                }
                            </div>
                        }
                        else {
                            return <div key={i} className="m-0 p-0">
                                <Link href={d.link} passHref>
                                    <a style={d.key === 4 ? { marginRight: 32 } : undefined} className={router.pathname === d.link ? "mx-4 text-active-lighter font-bold lg:text-sm xl:text-lg" : "mx-4  text-medium-grey lg:text-sm xl:text-lg font-roboto-medium"}>{d.name}</a>
                                </Link>
                            </div>
                        }
                    })
                }

            </div>
            {
                mobileDropDown ?
                    <div className="absolute top-20 left-0 w-full h-screen z-10 mobile-nav-dropdown bg-opacity-20 flex flex-col p-4">
                        {
                            menu.map((d, i) => (
                                d.subMenu ?
                                    <div>
                                        <Link href={'#'} key={i}>
                                            <div className="flex items-center mb-4"  onClick={() => setExpand(!expand)}>
                                            <a className="text-active-lighter text-lg font-bold mr-2">{d.name}</a>
                                            <img src='/chevron.png' className={expand ? 'rotate-180' : 'rotate-0'} alt='solution menu'/>
                                            </div>
                                        </Link>
                                        { expand && 
                                            d.subMenu.map((a) => (
                                                <Link href={a.link} key={a.key}>
                                                    <h1 className="text-active-lighter text-sm mb-4 cursor-pointer">{a.name}</h1>
                                                </Link>
                                            ))
                                        }
                                    </div>
                                    :
                                    <Link href={d.subMenu ? '/' : d.link} key={i}>
                                        <a className="text-active-lighter text-lg font-bold mb-4 block">{d.name}</a>
                                    </Link>
                            ))
                        }
                    </div> : null
            }
        </div>
    )
}
export default Navbar