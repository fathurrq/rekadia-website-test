import Link from 'next/link'
import { useEffect, useState } from 'react'
import { API_URL } from '../../lib/config'

const Footer = () => {
    const [instagram, setInstagram] = useState({})
    const [linkedin, setLinkedIn] = useState({})
    const [workshop, setWorkshop] = useState({})
    const [headquarter, setHeadQuarter] = useState({})
    const [email, setEmail] = useState({})
    const [phone, setPhone] = useState({})
    const [whatsapp, setWhatsapp] = useState({})

    useEffect(() => {
        fetch(API_URL + 'about-rekadia') .then(function(response) {
            return response.json();
          }).then(function(data) {``
              let [wa,ig,li,ws,hq,email,phone] = data
              setInstagram(ig)
              setLinkedIn(li)
              setWorkshop(ws)
              setHeadQuarter(hq)
              setEmail(email)
              setPhone(phone)
              setWhatsapp(wa)
          }).catch((e) => console.log(e))
      }, [])
    return (
        <div className="flex flex-col justify-center xl:h-80 lg:h-80 sm:h-auto xl:p-8 lg:p-8 sm:p-4">
            <div className="mb-4 xl:pt-6 lg:pt-6 sm:pt-0">
                <img src='/logo-black.png' alt='footer logo' />
            </div>
            <h2 className="text-light-grey font-bold mb-4 text-lg">PT. Rekadia Solusi Teknologi</h2>
            <div className="grid xl:grid-cols-3 lg:grid-cols-3 sm:grid-cols-4">
                <div className="xl:col-span-1 lg:col-span-1 sm:col-span-4">
                    <div>
                        <div className="flex items-center">
                            <img src='/pin.svg' alt='pin' />
                            <h2 className="text-light-grey text-sm mb-0">{headquarter?.title?.rendered || ''}</h2>
                        </div>
                        <div className="flex flex-col mb-3 ml-9">
                            <h2 className="text-light-grey text-xs">{headquarter?.acf?.description || ''}</h2>
                        </div>
                    </div>
                    <div>
                        <div className="flex items-center">
                            <img src='/pin.svg'  alt='pin'/>
                            <h2 className="text-light-grey text-sm mb-0">{workshop?.title?.rendered || ''}</h2>
                        </div>
                        <div className="flex flex-col mb-9 ml-9">
                            <h2 className="text-light-grey text-xs">
                            {workshop?.acf?.description || ''}
                            </h2>
                        </div>
                    </div>
                </div>
                <div className="flex xl:justify-center lg:justify-center sm:justify-start xl:col-span-1 lg:col-span-1 sm:col-span-2">
                    <div className="flex flex-col">
                        <h2 className="text-light-grey font-bold mb-4 text-lg">Connect With Us</h2>
                        <div className="flex mb-4 items-center">
                            <img src='/wa.svg'  alt='phone'/>
                            <h2 className="text-light-grey text-xs ml-2 mb-0">{phone?.acf?.description ||''}</h2>
                        </div>
                        <div className="flex mb-4 items-center">
                            <img src='/whatsapp.png' alt='whatsapp' />
                            <a target={"_blank"} href={`https://api.whatsapp.com/send?phone=${whatsapp?.acf?.description || ''}`} className="text-light-grey text-xs ml-2 mb-0">+{whatsapp?.acf?.description || ''}</a>
                        </div>
                        <div className="flex mb-4 items-center">
                            <img src='/message.svg' alt='message' />
                            <a href={`mailto:${email?.acf?.description || ''}`} className="text-light-grey text-xs ml-2 mb-0">{email?.acf?.description || ''}</a>
                        </div>
                        <div className="flex">
                            <Link href={instagram?.acf?.description || ''} passHref>
                                <a target={"_blank"} className="mr-2.5 cursor-pointer">
                                    <img alt='instagram' src={instagram?.better_featured_image?.source_url} height={24} width={24} />

                                </a>
                            </Link>
                            <Link href={linkedin?.acf?.description || ''} passHref>
                                <a target={"_blank"} className='cursor-pointer'>
                                    <img alt='linkedin' src={linkedin?.better_featured_image?.source_url} height={24} width={24} />

                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
                <div className="flex justify-center xl:col-span-1 lg:col-span-1 sm:col-span-2">
                    <div className="flex flex-col">
                        <a className="text-primary font-bold mb-2 text-lg" href={'/about-us'}>About Us</a>
                        <a className="text-primary font-bold mb-2 text-lg" href='#'>Solution</a>
                        <a className="text-primary font-bold text-lg" href={'/join-us'}>Career</a>
                    </div>
                </div>

            </div>
        </div>
    )
}
export default Footer