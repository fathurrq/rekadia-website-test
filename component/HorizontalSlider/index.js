import Carousel from 'react-elastic-carousel'
import { useEffect, useState } from 'react'

// const logo = [logo0, logo1, logo2, logo3, logo4, logo5, logo6, logo8, logo9, logo10, logo11, logo12, logo13, logo14]

const ImageSlider = (data) => {
    //state
    const [mobile, setMobile] = useState(false)
    useEffect(() => {
        if (window.innerWidth < 500) {
            setMobile(true)
        }
    }, [])
    //break point setting for Carousel
    const breakPoints = [
        { width: 1, itemsToShow: 3, pagination: false },
        { width: 550, itemsToShow: 5, pagination: false },
        { width: 850, itemsToShow: 5, pagination: false },
        { width: 1150, itemsToShow: 5, pagination: false },
        { width: 1450, itemsToShow: 8, pagination: false },
        { width: 1750, itemsToShow: 5, pagination: false },
    ]
    return (
        <Carousel breakPoints={breakPoints} enableAutoPlay={mobile} dots={true}>
            {
                //map data
                data.data.map((d, i) => (
                    <div className="sm:h-24 lg:h-32 xl:h-32 flex justify-center items-center overflow-hidden" key={i}>
                        <div 
                        className={d?.better_featured_image.media_details?.height < 1000 ? "lg:p-8 xl:p-8 2xl:p-8 sm:p-2" : "lg:p-8  xl:p-8 2xl:p-8 sm:p-6"}
                        >
                            <img src={d.better_featured_image.source_url} alt={d?.title?.rendered} />
                        </div>
                    </div>
                ))
            }
        </Carousel>
    )
}
export default ImageSlider