import Footer from 'component/layout/footer'
import Navbar from 'component/layout/navbar'
import Link from 'next/link'
import { Carousel } from 'antd';
import { useState, useEffect, useRef } from 'react'
import Head from 'next/head'
import { fetchSingleContent } from 'lib/fetch'
import { API_URL } from 'lib/config'
import { orderItems } from 'lib/order';
import { NextSeo } from 'next-seo'

export default function JoinUs({ header, ourMission, quote, earth, job, email, program, ourValue }) {
    const [isMobile, setIsMobile] = useState(false)
    const [bigScreen, setBigScreen] = useState(false)
    const [slider1, setSlider1] = useState(null)
    const [slider2, setSlider2] = useState(null)

    useEffect(() => {
        if (window.innerWidth > 1300) {
            setBigScreen(true)
        }
        else if (window.innerWidth < 500) {
            setIsMobile(true)
        }
    }, [])
    return (
        <div>
            <Head>
                {/* <NextSeo title="Join Us" /> */}
                <title>Career & Vacancy - Rekadia</title>
                <link rel="icon" href="/logo-rekadia.png" />

            </Head>
            <Navbar />
            <div style={{
                backgroundImage: `url(${isMobile ? '/header-join-mobile.png' : '/header-join.png'})`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover"
            }
            }
                className="xl:relative lg:relative sm:static block w-full xl:h-150 lg:h-150 sm:h-275 xl:p-0 lg:p-0 sm:p-4"
            >
                <div className="xl:absolute lg:absolute sm:static xl:block lg:block sm:flex flex-col sm:justify-center xl:h-auto lg:h-auto sm:h-275 left-7 lg:bottom-32">
                    <h1 className="xl:hidden lg:hidden sm:flex text-sm text-line-grey">{header.content.rendered.replace(/<\/?[^>]+>/gi, '')}</h1>
                    <h1 className="text-white mb-5 font-slab xl:text-header lg:text-header sm:text-lg w-785 xl:leading-tight lg:leading-tight sm:leading-relaxed">
                        {header.acf.description}
                    </h1>
                </div>
            </div>
            <div className="grid grid-cols-2 xl:h-440 lg:h-440 sm:h-auto" >
                <div className="gradient-bg flex flex-col items-center justify-center xl:col-span-1 lg:col-span-1 sm:col-span-2">
                    <p className=" text-white text-lg font-bold text-center mb-9 xl:text-2xl xl:mt-0 lg:mt-0 sm:mt-4">{ourMission.title.rendered}</p>
                    <p className="text-white xl:text-4xl lg:text-4xl sm:text-2xl text-center m-0 xl:mb-0 lg:mb-0 sm:mb-9 leading-tight xl:w-500 lg:w-500 sm:w-72" >{ourMission.content.rendered.replace(/<\/?[^>]+>/gi, '')}</p>
                </div>
                <div className=" flex items-center justify-center xl:col-span-1 lg:col-span-1 sm:col-span-2">
                    <p className="xl:text-4xl lg:text-4xl sm:text-lg text-center text-light-grey m-0 xl:pt-4 lg:pt-4 sm:pt-12 xl:pb-0 lg:pb-0 sm:pb-12 leading-tight xl:w-500 lg:w-500 sm:w-72 xl:font-normal lg:font-normal sm:font-bold xl:leading-normal lg:leading-normal sm:leading-relaxed">{quote.content.rendered.replace(/<\/?[^>]+>/gi, '')}</p>
                </div>
            </div>
            <div className="grid grid-cols-2 xl:h-440 lg:h-440 sm:h-auto">
                <div className="flex flex-col items-center justify-center xl:col-span-1 lg:col-span-1 sm:col-span-2 xl:order-first lg:order-first sm:order-last">
                    <p className="xl:mx-0 lg:mx-0 sm:mx-4 xl:text-4xl lg:text-4xl sm:text-lg text-center text-light-grey m-0 xl:pt-4 lg:pt-4 sm:pt-12 xl:pb-0 lg:pb-0 sm:pb-12 xl:leading-tight lg:leading-tight sm:leading-relaxed xl:w-500 lg:w-500 sm:w-auto xl:font-normal lg:font-normal sm:font-bold">{quote.acf.description}</p>
                </div>
                <div className="bg-light-green xl:flex lg:flex sm:flex items-center justify-center  xl:col-span-1 lg:col-span-1 sm:col-span-2 xl:order-last lg:order-last sm:order-first p-6">
                    <img src={ourValue.better_featured_image.source_url} />
                </div>
            </div>
            <div className="grid grid-cols-2" >
                <div className=" items-center justify-center bg-darkest-blue xl:col-span-1 lg:col-span-1 sm:col-span-2 xl:order-first lg:order-first sm:order-last">
                    <Carousel autoplay={true} dots={false} asNavFor={slider2} ref={setSlider1}>
                        {
                            program.map((d) => (
                                <img src={d.better_featured_image.source_url} />
                            ))
                        }

                    </Carousel>
                </div>
                <div className=" items-center bg-darkest-blue xl:col-span-1 lg:col-span-1 sm:col-span-2 xl:order-last lg:order-last sm:order-first ">
                    {/* <h2 className="text-2xl font-bold text-white text-center mt-24 mb-6 pb-0">Rekadian Program</h2> */}
                    <div className="xl:mt-24 lg:mt-24 sm:mt-2 ">
                        <Carousel autoplay={true} asNavFor={slider1} ref={setSlider2}>
                            {
                                program.map((d) => (
                                    <div className="flex items-center justify-center bg-darkest-blue text-center">
                                        <h2 className="xl:text-4xl lg:text-4xl sm:text-lg text-white xl:h-24 lg:h-24 sm:h-16 xl:font-normal lg:font-normal sm:font-bold mb-0">{d.title.rendered}</h2>
                                    </div>
                                ))
                            }
                        </Carousel>
                        <div className='xl:hidden lg:hidden sm:block h-4 bg-darkest-blue' />
                    </div>
                </div>
            </div>
            <div className="xl:h-85 lg:h-85 sm:h-auto xl:p-7 lg:p-7 sm:p-4" >
                <div className="w-full flex flex-col items-center xl:border lg:border sm:border-0 xl:border-light-green lg:border-light-green xl:h-72 lg:h-72 sm:h-auto relative">
                    <div className="absolute xl:block lg:block sm:hidden left-0">
                        <img src='/bumi_kiri.png' />
                    </div>
                    <h2 className="text-2xl text-dark font-bold text-center xl:mt-10 lg:mt-10 sm:mt-9 xl:mb-6 lg:mb-6 sm:mb-11">{earth.title.rendered}</h2>
                    <p className="text-light-grey font-medium text-center xl:w-2/5 lg:w-2/5 sm:w-full">
                        {earth.acf.description}
                    </p>
                    <div className="absolute xl:block lg:block sm:hidden right-0" >
                        <img src='/bumi_kanan.png' />
                    </div>
                </div>
            </div>
            <div className="xl:p-6 lg:p-6 sm:p-4 w-full bg-darker-blue xl:items-center lg:items-center sm:items-start flex flex-col relative xl:z-50 lg:z-50 sm:z-0">
                <h2 className="xl:pt-0 lg:pt-0 sm:pt-4 xl:leading-normal lg:leading-normal sm:leading-relaxed xl:text-center lg:text-center sm:text-left xl:text-4xl lg:text-4xl sm:text-lg text-white xl:mb-12 lg:mb-12 sm:mb-3 leading-tight xl:w-500 lg:w-500 sm:w-64 xl:font-normal lg:font-normal sm:font-bold">
                    You have the opportunity to be part of Rekadian NOW!
                </h2>
                <div className="grid grid-cols-2 w-full">
                    <div className="py-5 xl:col-span-1 lg:col-span-1 sm:col-span-2">
                        <h2 className="text-white text-lg font-roboto-medium xl:flex lg:flex sm:hidden">Open Position</h2>
                        {
                            job.map((d, i) => (
                                <div key={i} className="flex xl:flex-row lg:flex-row sm:flex-col w-full justify-between mb-4 xl:items-center lg:items-center sm:items-start">
                                    <p className="text-white font-bold text-2xl m-0 p-0 xl:mb-0 lg:mb-0 sm:mb-2" >{d.title.rendered}</p>
                                    <Link href={d.acf.description}>
                                        <button className="p-4 text-white bg-primary font-bold text-base ">Download Brochure</button>
                                    </Link>
                                </div>
                            ))
                        }
                    </div>
                    <div className="xl:px-10 lg:px-10 sm:px-0 xl:py-5 lg:py-5 sm:py-4 xl:col-span-1 lg:col-span-1 sm:col-span-2">
                        <h2 className="text-white font-roboto-medium xl:mb-2 lg:mb-2 sm:mb-0 text-lg">Send your CV to</h2>
                        <p className="text-line-grey font-bold text-2xl mb-0 ">{email.acf.description}</p>
                        <p className="text-white m-0">and we will reply within 2 business days.</p>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export async function getStaticProps() {
    //Job Content
    const jobData = await fetch(API_URL + 'job-vacancy')
    const job = await jobData.json()
    const rekadiaProgram = await fetch(API_URL + 'rekadia-program')
    const program = await rekadiaProgram.json()
    orderItems(program)
    //Join Us content 
    const header = await fetchSingleContent(API_URL + 'join-us-content?slug=header')
    const quote = await fetchSingleContent(API_URL + 'join-us-content?slug=join-us-quote')
    const ourMission = await fetchSingleContent(API_URL + 'about-us-content?slug=our-mission')
    const ourValue = await fetchSingleContent(API_URL + 'about-us-content?slug=our-value')
    const email = await fetchSingleContent(API_URL + 'about-rekadia?slug=email')
    const earth = await fetchSingleContent(API_URL + 'join-us-content?slug=we-love-earth')
    return {
        props: {
            header,
            ourMission,
            quote,
            earth,
            job,
            email,
            program,
            ourValue
        },
        revalidate: 10
    }
}