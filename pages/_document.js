import React from 'react'
import Document, { Html, Head, Main, NextScript } from 'next/document'

const isProduction = process.env.NODE_ENV === 'production'

class MyDocument extends Document {
    render() {

        return (
            <Html>
                <Head>
                    <script async src={`https://www.googletagmanager.com/gtag/js?id=UA-3296049-13`} />
                    <script
                        dangerouslySetInnerHTML={{
                            __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-3296049-13', {
              page_path: window.location.pathname,
            });
          `
                        }}
                    />
                    <link
                        href="https://fonts.googleapis.com/css2?family=Roboto+Slab"
                        rel="stylesheet"
                    />
                    <meta name="robots" content="all" />

                </Head>

                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}

export default MyDocument
