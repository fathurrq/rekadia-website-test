
import Slider from '../component/HorizontalSlider'
import Navbar from '../component/layout/navbar'
import Footer from '../component/layout/footer'
import Link from 'next/link'
import { useEffect, useState } from 'react'
// import Carousel from 'react-elastic-carousel'
import Head from 'next/head'
import { API_URL } from '../lib/config'
import { orderItems } from '../lib/order'
import { fetchSingleContent } from '../lib/fetch'
import { Carousel } from 'antd'
import { NextSeo } from 'next-seo'

const Home = ({ posts = [], services = [], technology = [], headerTitle = {}, speciality = {}, featuredSolution = {}, bottomContent = {}, computerImage = {}, map = {} }) => {
  const [menuActive, setMenuActive] = useState(0)
  const [bigScreen, setBigScreen] = useState(false)
  const [isMobile, setIsMobile] = useState(false)

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      }
    ]
  };
  useEffect(() => {
    if (window.innerWidth > 1300) {
      setBigScreen(true)
    }
    else if (window.innerWidth < 500) {
      setIsMobile(true)
    }
  }, [])
  return (
    <div className="w-full flex flex-col">
      <Head>
        {/* <NextSeo title="PT Rekadia Solusi Teknologi - IT Solution Provider" /> */}
        <title>PT Rekadia Solusi Teknologi - IT Solution Provider</title>
        <link rel="icon" href="/logo-rekadia.png" />
      </Head>
      <Navbar />
      <div className="w-full xl:h-150 lg:h-150 sm:h-auto"
        style={{
          backgroundImage: `url('/header.png')`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover"
        }
        }
      >
        <video className="xl:relative lg:relative sm:static w-full" height="auto"
          poster="" muted loop preload="true" autoPlay
        >
          <source src='/home-footage.mp4' type="video/mp4" />
        </video>
      </div>
      <div className="mobile-bg-blue xl:absolute xl:left-7 xl:z-30 lg:absolute lg:left-7 lg:z-30 sm:static xl:bg-transparent lg:bg-transparent xl:p-0 lg:p-0 sm:p-4 xl:pt-0 lg:pt-0 sm:pt-2.5" style={bigScreen ? { top: 256 } : { top: 206 }}>
        <h2 className="xl:text-white lg:text-white sm:text-line-grey lg:text-2xl xl:text-2xl xl:mb-6 lg:mb-6 sm:mb-4">
          {headerTitle?.content?.rendered?.replace(/<\/?[^>]+>/gi, '')}
        </h2>
        <h1 className="text-white  lg:w-4/5 xl:w-4/5 space-x-2 space-y-8 tracking-wider xl:leading-tight lg:leading-tight sm:leading-relaxed xl:mb-6 lg:mb-6 sm:mb-0 font-slab xl:text-header lg:text-header sm:text-lg xl:font-normal lg:font-normal sm:font-bold">
          {headerTitle?.acf?.description}
        </h1>
        <Link href={'/contact-us'} passHref>
          <button className=" p-4 text-white bg-primary font-bold text-base xl:flex lg:flex sm:hidden">Contact Us</button>
        </Link>
      </div>
      <div className="w-full xl:flex lg:flex bg-darker justify-center relative flex-col">
        <h1 className="text-white xl:text-lg lg:text-lg sm:text-sm xl:font-bold lg:font-bold sm:font-normal my-6 xl:ml-16 lg:ml-16 sm:ml-0  pl-4 pr-16 ">Services</h1>
        <div className="w-full flex xl:flex-row lg:flex-row sm:flex-col">
          <div className="flex flex-col items-start xl:mx-16 lg:mx-16 sm:mx-4 xl:pb-12 lg:pb-12 sm:pb-4">
            {
              services.map((d, i) => {
                return (
                  <div className='w-full'>

                    <button key={i} onClick={() => setMenuActive(i)} className={(menuActive === i) ?
                      `w-full text-left text-white xl:text-2xl lg:text-2xl sm:text-lg bg-primary p-4 xl:py-4 
                  lg:py-4 sm:py-4 font-bold xl:mb-0 lg:mb-0 sm:mb-2` : `xl:mb-0 lg:mb-0 sm:mb-2 w-full text-left 
                  text-primary xl:text-2xl lg:text-2xl sm:text-lg p-4 xl:py-4 lg:py-4 sm:py-1 font-bold 
                  hover:text-white hover:bg-primary`}>{d?.title?.rendered}</button>
                    {
                      (menuActive === i) ?
                        <div className="sm:flex lg:hidden xl:hidden flex-col xl:w-96 lg:w-96 sm:w-full sm:p-0  sm:pl-0 xl:p-0 lg:p-0 ">
                          <p className="xl:w-80 lg:w-80 sm:w-full text-line-grey lg:text-sm xl:text-sm sm:text-xs mt-2">{services?.[menuActive]?.content?.rendered?.replace(/<\/?[^>]+>/gi, '')}</p>
                          {/* <div className="flex justify-end mb-2">
                            <a className="text-primary font-roboto-medium mr-2 text-xs">
                              See More
                            </a>
                            <img src='/arrow.png' />
                          </div> */}
                        </div>

                        : null
                    }
                  </div>
                )
              })
            }

          </div>
          <div className="xl:flex lg:flex sm:hidden flex-col xl:w-96 lg:w-96 sm:w-full sm:p-4 xl:p-0 lg:p-0">
            <p className="xl:w-80 lg:w-80 sm:w-full text-line-grey lg:text-sm xl:text-sm">{services?.[menuActive]?.content?.rendered?.replace(/<\/?[^>]+>/gi, '')}</p>
            {/* <div className="flex justify-end">
              <a className="text-primary font-roboto-medium mr-2">
                See More
              </a>
              <img src='/arrow.png' />
            </div> */}
          </div>
        </div>
        <div className="absolute right-0 xl:block lg:block sm:hidden " style={{ bottom: "-45px" }}>
          <img src={computerImage?.better_featured_image?.source_url} alt='computer' />
        </div>
      </div>
      <div className="flex flex-col w-full">
        <div className="flex xl:pt-9 lg:pt-9 sm:pt-4 xl:ml-16 lg:ml-16 sm:ml-4 xl:mr-0 lg:mr-0 sm:mr-4 xl:flex-row lg:flex-row sm:flex-col xl:h-auto lg:h-auto sm:h-auto xl:mb-28 lg:mb-20">
          <h1 className="xl:text-4xl lg:text-4xl sm:text-sm text-dark lg:mr-28 xl:mb-0 lg:mb-0 sm:mb-2.5 sm:text-center xl:text-left lg:text-left" >Technology {isMobile ? undefined : <br />} Expertise</h1>
          <div className="flex justify-center slider-container lg:mt-4 xl:mt-0">
            <Carousel {...settings} autoplay={true} variableWidth={true}>
              {
                technology.map((d) => (
                  // <div>
                  <div className="h-auto relative xl:mr-25 lg:mr-16 sm:mr-8">
                    <img src={d?.better_featured_image?.source_url} alt={d?.title?.rendered}
                      // className={d?.better_featured_image.media_details?.height > d?.better_featured_image.media_details?.width ? 'w-full h-16 ' : 'w-full h-16'}
                      // className='xl:h-20 lg:h-20 sm:h-20'
                      style={(isMobile && { height: 48 }) || (bigScreen && { height: 84 }) || (!bigScreen && !isMobile && { height: 84 })}
                    />
                  </div>
                  // </div>
                ))
              }
            </Carousel>
          </div>
          <div className='w-7 xl:block lg:block sm:hidden' />
        </div>
        <div className="w-full xl:px-6 lg:px-6 sm:px-0 xl:relative lg:relative sm:static">
          <div className="absolute left-0 z-50 xl:block lg:block sm:hidden" style={{ bottom: -20 }}>
            <img src={speciality?.better_featured_image?.source_url} width={469} height={407} />
          </div>
          <div className=" w-full h-auto grid xl:grid-cols-2 lg:grid-cols-2 sm:grid-cols-1 xl:gap-8 lg:gap-8 sm:gap-0 gradient-bg">
            <div className="xl:flex lg:flex sm:hidden"></div>
            <div className="lg:mr-8 sm:mr-4 xl:ml-0 lg:ml-0 sm:ml-4">
              <h1 className="text-white lg:text-4xl xl:text-4xl sm:text-sm xl:mb-9 lg:mb-9 sm:mb-4  xl:mt-14 lg:mt-14 sm:mt-4">{speciality?.content?.rendered?.replace(/<\/?[^>]+>/gi, '')}</h1>
              <h1 className="text-line-grey lg:text-lg xl:text-lg sm:text-lg xl:mt-0 lg:mt-0 sm:mt-0 font-bold xl:w-auto lg:w-auto sm:w-full xl:mb-14x lg:mb-12 sm:mb-4">
                {speciality?.acf?.description}
              </h1>
            </div>
          </div>
        </div>
      </div>
      <div className="xl:relative lg:relative sm:static xl:pt-11 lg:pt-11 sm:pt-4 px-0 bg-line-grey sm:flex sm:flex-col xl:h-530 lg:h-530 sm:h-auto">
        <div className="absolute xl:block lg:block sm:hidden right-0 lg:top-64 xl:top-56 z-0" style={{ width: 256, height: 367 }}>
          <img src='/abstract-motive.png' alt='abstract' />
        </div>
        <div className="flex flex-col items-center justify-center">
          <h2 className="text-light-grey xl:font-bold lg:font-bold sm:font-normal mb-2 lg:text-lg xl:text-xl sm:text-xs xl:hidden lg:hidden sm:flex px-4 text-center">{featuredSolution?.title?.rendered}</h2>
          <h2 className="text-dark font-bold lg:text-2xl xl:text-3xl sm:text-lg xl:hidden lg:hidden sm:flex px-4 sm:mb-6 text-center">{featuredSolution?.content?.rendered?.replace(/<\/?[^>]+>/gi, '')}</h2>
        </div>
        <div className="xl:absolute lg:absolute sm:static l-0 xl:w-2/5 2xl:w-1/3 lg:w-1/2 sm:w-full pl-4 sm:pr-4 xl:pr-0 lg:pr-0">
          <img src={featuredSolution?.better_featured_image?.source_url} alt='featured image' />
        </div>
        <div className="w-full grid xl:grid-cols-2 lg:grid-cols-2 sm:grid-cols-1 lg:gap-0 xl:gap-0 xl:h-110 lg:h-110 sm:h-auto z-50">
          <div className="sm:hidden xl:block lg:block"></div>
          <div className="flex flex-col p-4">
            <h2 className="text-light-grey font-bold mb-2 lg:text-lg xl:text-lg  xl:flex lg:flex sm:hidden">{featuredSolution?.title?.rendered}</h2>
            <h2 className="text-dark font-bold text-2xl xl:flex lg:flex sm:hidden">{featuredSolution?.content?.rendered?.replace(/<\/?[^>]+>/gi, '')}</h2>
            <div>
              <h2 className="xl:text-darker-blue lg:text-darker-blue sm:text-light-grey mb-5 text-sm ">
                {featuredSolution?.acf?.description}
              </h2>
              <Link href={'/solution/maintenance'} passHref>
                <a className="w-full flex items-center font-bold text-primary justify-end pr-4">
                    See More
                  <img src='/arrow.png' className='ml-2' alt='arrow'/>
                </a>
              </Link>
            </div>
          </div>

        </div>
      </div>
      <div className="block px-0 lg:mt-0 xl:mt-0 sm:mt-2.5 w-full">
        <h2 className="xl:font-bold text-center lg:font-bold sm:font-normal xl:text-2xl lg:text-2xl sm:text-sm xl:mt-20 lg:mt-20 sm:mt-2.5 text-dark mb-0">Who's Already Trusting Us</h2>
        <div>
          <Slider data={posts} />
        </div>
      </div>
      <div className="lg:h-16 xl:h-16 sm:h-0" />
      <div className="w-full bg-darker-blue grid grid-cols-2">
        <div className="flex justify-center items-center flex-col xl:col-span-1 lg:col-span-1 sm:col-span-2">
          <h2 className="text-white leading-tight w-80 text-center xl:mb-6 lg:mb-6 sm:mb-4 xl:mt-0 lg:mt-0 sm:mt-7 font-slab text-header">
            {bottomContent?.content?.rendered?.replace(/<\/?[^>]+>/gi, '')}
          </h2>
          <Link href={'/contact-us'}>
            <button className="p-4 text-white bg-primary font-bold text-base ">Contact Us</button>
          </Link>
        </div>
        <div className="flex justify-center items-center p-4 xl:py-6 lg:py-6 sm:py-4  xl:col-span-1 lg:col-span-1 sm:col-span-2">
          <div className="w-full">
            <img src={map?.better_featured_image?.source_url} alt='map'/>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  )
}
export async function getStaticProps() {
  //Client logo
  const res = await fetch(API_URL + 'client-logo?per_page=100')
  const posts = await res.json()
  orderItems(posts)
  //Services
  const serv = await fetch(API_URL + 'services')
  const services = await serv.json()
  orderItems(services)

  //Technology Expertise 
  const tech = await fetch(API_URL + 'technology-expertise')
  const technology = await tech.json()


  const test = await fetch(API_URL + 'home-content')
  //dont change the array order
  const [speciality,bottomContent,featuredSolution,headerTitle,computerImage ] = await test.json()
  const map = await fetchSingleContent(API_URL + 'about-rekadia?slug=head-quarter')
  return {
    props: {
      posts,
      services,
      technology,
      headerTitle,
      speciality,
      featuredSolution,
      bottomContent,
      computerImage,
      map
    },
    revalidate: 10
  }
}
export default Home