import 'tailwindcss/tailwind.css'
import 'styles/global.css'
import 'antd/dist/antd.css';
import SEO from 'next-seo.config'
import { DefaultSeo } from 'next-seo'

// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
function MyApp({ Component, pageProps }) {
  return (
    <>
      <DefaultSeo {...SEO} />
      <Component {...pageProps} />
    </>
  )
}
export default MyApp
