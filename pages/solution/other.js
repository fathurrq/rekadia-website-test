
import Footer from 'component/layout/footer'
import Navbar from 'component/layout/navbar'
import Carousel from 'react-elastic-carousel'
import { Carousel as Slider } from 'antd';

import Link from 'next/link'
import { useState, useRef, useEffect } from 'react';
import Head from 'next/head'
import { fetchMenuContent, fetchSingleContent } from 'lib/fetch';
import { API_URL } from 'lib/config';

export default function Other(
    {
        header,
        stepContent,
        businessIntelligent,
        eProcurement,
        documentManagement,
        mobileWorkforce,
        projectManagement,
        businessIntelligentTech,
        eProcurementTech,
        documentManagementTech,
        mobileWorkforceTech,
        projectManagementTech,
        customSoftware
    }) {
    const slider = useRef(null);
    const [menuActive, setMenuActive] = useState(0)
    const [isMobile, setIsMobile] = useState(false)
    const [bigScreen, setBigScreen] = useState(false)
    const [slideIndex, setSlideIndex] = useState(0)
    useEffect(() => {
        if (window.innerWidth > 1300) {
            setBigScreen(true)
        }
        else if (window.innerWidth < 500) {
            setIsMobile(true)
        }
    }, [])

    return (
        <div>
            <Head>
                {/* <NextSeo title="Solution - Other" /> */}
                <title>Custom Application - Rekadia</title>
                <link rel="icon" href="/logo-rekadia.png" />

            </Head>
            <Navbar />
            <div style={{
                backgroundImage: `url(${isMobile ? '/header-prod-mobile.png' : '/header-other.jpg'})`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
            }
            }
                className="relative w-full xl:block lg:block sm:block xl:h-500 lg:h-500 sm:h-275"
            >
                <div className="xl:absolute lg:absolute sm:static xl:p-0 lg:p-0 sm:p-4 xl:pt-0 lg:pt-0 sm:pt-6 left-7 bottom-28">
                    <h1 className="text-whitespace-x-2 space-y-8 m-0 xl:text-header lg:text-header sm:text-2xl font-slab text-white xl:mb-1 lg:mb-1 sm:mb-4 ">
                        {header.content.rendered.replace(/<\/?[^>]+>/gi, '')}
                    </h1>
                    <h2 className="text-white text-sm font-sans xl:leading-normal lg:leading-normal sm:leading-snug" >
                        {header.acf.description}
                    </h2>
                </div>
            </div>
            <h2 className="text-dark lg:text-4xl xl:text-5xl sm:text-lg xl:font-normal lg:font-normal sm:font-bold my-6 mx-6 xl:text-left lg:text-left sm:text-center" >
                How it Works?
            </h2>
            <div className="flex px-6 mb-4 w-full pt-8 ">
                <div className='sm:flex lg:hidden xl:hidden items-center justify-center '>
                    <button onClick={slideIndex == 0 ? null : () => slider.current.slidePrev()} className={slideIndex == 0 ? 'flex outline-none rounded-full text-2xl text-light-grey bg-white' : 'flex outline-none rounded-full text-2xl text-active bg-white'}>{'<'}</button>
                </div>
                <Carousel ref={slider} autoplay={true} dots={false} showArrows={false} onChange={(d) => setSlideIndex(d.index)} breakPoints={
                    [
                        { width: 1, itemsToShow: 1, pagination: false },
                        { width: 550, itemsToShow: 1, pagination: false },
                        { width: 850, itemsToShow: 5, pagination: false },
                        { width: 1150, itemsToShow: 5, pagination: false },
                        { width: 1450, itemsToShow: 5, pagination: false },
                        { width: 1750, itemsToShow: 5, pagination: false },
                    ]
                }>
                    {
                        stepContent.map((d) => (
                            <div className='h-auto w-full'>
                                <div className="flex flex-col items-center">
                                    <img src={d.better_featured_image.source_url} />
                                    <div className={`xl:mt-6 lg:mt-6 sm:mt-8 ${d.content.rendered.length > 25 ? 'w-40' : 'w-32'}`} style={{ borderBottom: "3px solid #4ACFFF" }}>
                                        <p className="text-sm text-dark text-center m-0 mb-2.5">{d.title.rendered}</p>
                                        <p className="text-lg text-dark text-center font-bold m-1 mb-2.5">{d.content.rendered.replace(/<\/?[^>]+>/gi, '')}</p>
                                    </div>
                                    <p className="text-sm text-dark text-center mt-2.5 w-40">
                                        {d.acf.description}
                                    </p>
                                </div>
                            </div>
                        ))
                    }
                </Carousel>
                <div className='sm:flex lg:hidden xl:hidden items-center justify-center'>
                    <button onClick={slideIndex == 4 ? null : () => slider.current.slideNext()} className={slideIndex == 4 ? 'flex outline-none rounded-full text-2xl text-light-grey bg-white' : ' rounded-full outline-none text-2xl text-active bg-white '}>{'>'}</button>
                </div>
            </div>
            <div className="xl:bg-line-grey lg:bg-line-grey sm:bg-white xl:px-7 lg:px-7 sm:px-4 xl:pl-12 lg:pl-12 sm:pl-4  xl:gap-7 lg:gap-7 sm:gap-0 xl:mt-20 lg:mt-20 sm:mt-0 xl:pb-12 lg:pb-12 sm:pb-4 xl:pt-6 lg:pt-6 sm:pt-4">
                <div className="grid grid-cols-2">
                    <div className="xl:col-span-1 lg:col-span-1 sm:col-span-2">
                        <h2 className="text-dark xl:text-4xl lg:text-4xl sm:text-2xl mb-6 xl:text-left lg:text-left sm:text-center">Custom Software Example</h2>
                        {
                            customSoftware.map((d, i) => (
                                <h4 className={`${menuActive === i ? "text-light-grey" : "text-active-lighter"} mb-2 border-b-2 border-black border-opacity-10 text-lg font-roboto-medium cursor-pointer `} onClick={() => setMenuActive(i)}>{d.title.rendered}</h4>
                            ))
                        }

                        <h2 className="xl:block lg:block sm:hidden font-bold text-dark xl:text-2xl lg:text-2xl sm:text-lg xl:text-left lg:text-left sm:text-center mb-4">Technology</h2>
                        <div className="flex items-center xl:flex lg:flex sm:hidden h-16">
                             {
                                menuActive === 0 && businessIntelligentTech.map((d) =>
                                    <div className="flex w-24 items-center justify-center">
                                        <img src={d.better_featured_image.source_url} />
                                    </div>
                                )
                            }
                            {
                                menuActive === 1 && eProcurementTech.map((d) =>
                                    <div className="flex w-24 items-center justify-center">
                                        <img src={d.better_featured_image.source_url} />
                                    </div>
                                )
                            }
                            {
                                menuActive === 2 && documentManagementTech.map((d) =>
                                    <div className="flex w-24 items-center justify-center">
                                        <img src={d.better_featured_image.source_url} />
                                    </div>
                                )
                            }
                            {
                                menuActive === 3 && mobileWorkforceTech.map((d) =>
                                    <div className="flex w-24 items-center justify-center">
                                        <img src={d.better_featured_image.source_url} />
                                    </div>
                                )
                            }
                            {
                                menuActive === 4 && projectManagementTech.map((d) =>
                                    <div className="flex w-24 items-center justify-center">
                                        <img src={d.better_featured_image.source_url} />
                                    </div>
                                )
                            }
                        </div>
                    </div>
                    <div className="mt-12 xl:col-span-1 lg:col-span-1 sm:col-span-2">
                        {
                            menuActive === 0 &&
                            <Slider autoplay={true} style={{ color: "blue" }} dotPosition='bottom'>
                                {
                                    businessIntelligent.map((d) =>
                                        d?.better_featured_image.media_details?.height > d?.better_featured_image.media_details?.width ?

                                            <div className='flex items-center justify-center'>
                                                <div className='h-auto p-4 flex items-center justify-center'>
                                                    <img src={d.better_featured_image.source_url} className=' lg:w-2/5 sm:w-2/5 xl:w-2/5' />
                                                </div>
                                            </div>
                                            :
                                            <div className='flex items-center justify-center'>
                                                <div className='h-auto p-4 flex items-center justify-center'>
                                                    <img src={d.better_featured_image.source_url} />
                                                </div>
                                            </div>
                                    )
                                }
                            </Slider>
                        }
                        {
                            menuActive === 1 &&
                            <Slider autoplay={true} style={{ color: "blue" }}>
                                {
                                    eProcurement.map((d) => (
                                        <div>
                                            <div className='h-auto p-4 flex items-center justify-center'>
                                                <img src={d.better_featured_image.source_url} />
                                            </div>
                                        </div>
                                    ))
                                }

                            </Slider>
                        }
                        {
                            menuActive === 2 &&
                            <Slider autoplay={true} style={{ color: "blue" }}>
                                {
                                    documentManagement.map((d) => (
                                        <div>
                                            <div className='h-auto p-4 flex items-center justify-center'>
                                                <img src={d.better_featured_image.source_url} />
                                            </div>
                                        </div>
                                    ))
                                }

                            </Slider>
                        }
                        {
                            menuActive === 3 &&
                            <Slider autoplay={true} style={{ color: "blue" }}>
                                {
                                    mobileWorkforce.map((d) => (
                                        <div>

                                            <div className='h-auto p-4 flex items-center justify-center'>
                                                <img src={d.better_featured_image.source_url} />
                                            </div>
                                        </div>
                                    ))
                                }
                            </Slider>
                        }
                        {
                            menuActive === 4 &&
                            <Slider autoplay={true} style={{ color: "blue" }}>
                                {
                                    projectManagement.map((d) => (
                                        <div>
                                            <div className='h-auto p-4 flex items-center justify-center'>
                                                <img src={d.better_featured_image.source_url} />
                                            </div>
                                        </div>
                                    ))
                                }
                            </Slider>
                        }
                    </div>
                    <div className='xl:hidden lg:hidden sm:block col-span-2'>
                        <h2 className="xl:hidden lg:hidden sm:block font-bold text-dark xl:text-2xl lg:text-2xl sm:text-lg xl:text-left lg:text-left sm:text-center my-4">Technology</h2>
                        <div className=" items-center justify-center xl:hidden lg:hidden sm:flex h-16">
                            {
                                menuActive === 0 && businessIntelligentTech.map((d) =>
                                    <div className="flex w-24 items-center justify-center">
                                        <img src={d.better_featured_image.source_url} />
                                    </div>
                                )
                            }
                            {
                                menuActive === 1 && eProcurementTech.map((d) =>
                                    <div className="flex w-24 items-center justify-center">
                                        <img src={d.better_featured_image.source_url} />
                                    </div>
                                )
                            }
                            {
                                menuActive === 2 && documentManagementTech.map((d) =>
                                    <div className="flex w-24 items-center justify-center">
                                        <img src={d.better_featured_image.source_url} />
                                    </div>
                                )
                            }
                            {
                                menuActive === 3 && mobileWorkforceTech.map((d) =>
                                    <div className="flex w-24 items-center justify-center">
                                        <img src={d.better_featured_image.source_url} />
                                    </div>
                                )
                            }
                            {
                                menuActive === 4 && projectManagementTech.map((d) =>
                                    <div className="flex w-24 items-center justify-center">
                                        <img src={d.better_featured_image.source_url} />
                                    </div>
                                )
                            }
                        </div>
                    </div>
                </div>
            </div>
            <div className="flex items-center justify-center bg-darker-blue lg:py-11 w-full xl:py-20 sm:py-9">
                <Link href={'/contact-us'}>
                    <button className="p-4  text-white bg-primary font-bold text-base">Contact Us</button>
                </Link>
            </div>
            <Footer />
        </div>
    )
}

export async function getStaticProps() {
    //Header
    const header = await fetchSingleContent(API_URL + 'other-content?slug=header')
    const stepContent = await fetchMenuContent(API_URL, 'how-it-works')
    const customSoftware = await fetchMenuContent(API_URL, 'custom-software-menu')


    const businessIntelligent = await fetchMenuContent(API_URL, 'business-inteligent-dashboard')
    const eProcurement = await fetchMenuContent(API_URL, 'e-procurement-software')
    const documentManagement = await fetchMenuContent(API_URL, 'document-management-system')
    const mobileWorkforce = await fetchMenuContent(API_URL, 'mobile-workforce-management')
    const projectManagement = await fetchMenuContent(API_URL, 'project-management-software')

    //Technology
    const businessIntelligentTech = await fetchMenuContent(API_URL, 'business-inteligent-dashboard-technology')
    const eProcurementTech = await fetchMenuContent(API_URL, 'e-procurement-software-technology')
    const documentManagementTech = await fetchMenuContent(API_URL, 'document-management-system-technology')
    const mobileWorkforceTech = await fetchMenuContent(API_URL, 'mobile-workforce-management-technology')
    const projectManagementTech = await fetchMenuContent(API_URL, 'project-management-software-technology')

    return {
        props: {
            header,
            stepContent,
            businessIntelligent,
            eProcurement,
            documentManagement,
            mobileWorkforce,
            projectManagement,
            businessIntelligentTech,
            eProcurementTech,
            documentManagementTech,
            mobileWorkforceTech,
            projectManagementTech,
            customSoftware
        },
        revalidate: 10
    }
}