import Footer from 'component/layout/footer'
import Navbar from 'component/layout/navbar'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import Head from 'next/head'
import { fetchSingleContent } from 'lib/fetch'
import { API_URL } from 'lib/config'
import { orderItems } from 'lib/order'
import { NextSeo } from 'next-seo'

export default function Maintenance({ header, textContent, feature, technology }) {
    const [expand, setExpand] = useState(false)
    const [isMobile, setIsMobile] = useState(false)
    const [bigScreen, setBigScreen] = useState(false)

    useEffect(() => {
        if (window.innerWidth > 1300) {
            setBigScreen(true)
        }
        else if (window.innerWidth < 500) {
            setIsMobile(true)
        }
    }, [])
    return (
        <div>
            <Head>
                {/* <NextSeo title="Solution - Maintenance" /> */}
                <title> Maintenance & Inspection Software - Rekadia</title>

                <link rel="icon" href="/logo-rekadia.png" />

            </Head>
            <Navbar />
            <div style={{
                backgroundImage: `url(${isMobile ? '/header-maintenance-mobile.png' : '/maintenance-content.png'})`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
            }
            }
                className="relative w-full flex xl:h-500 lg:h-500 sm:h-275"
            >
                <div className="xl:absolute lg:absolute sm:static xl:p-0 lg:p-0 sm:p-4 justify-center flex flex-col left-7 bottom-28">
                    <h1 className="text-white space-x-2 space-y-8  m-0 xl:mb-0 lg:mb-0 sm:mb-4 font-slab xl:text-header lg:text-header sm:text-2xl xl:leading-normal lg:leading-normal sm:leading-tight">
                        {header.content.rendered.replace(/<\/?[^>]+>/gi, '')}
                    </h1>
                    <h2 className="text-white lg:text-sm xl:text-md font-sans ">{header.acf.description}</h2>
                </div>
            </div>

            <div className="w-full flex justify-center relative flex-col xl:px-7 lg:px-7 sm:px-4 xl:pt-9 lg:pt-9 sm:pt-4 xl:mb-30 lg:mb-30 sm:mb-0">
                <p className="xl:w-3/5 lg:w-3/5 sm:w-auto xl:text-left lg:text-left sm:text-center text-sm text-light-grey leading-snug">
                    {textContent.content.rendered.replace(/<\/?[^>]+>/gi, '')}
                </p>
                <p className={` xl:block lg:block sm:${expand ? 'block' : 'hidden'} xl:w-3/5 lg:w-3/5 sm:w-auto xl:text-left lg:text-left sm:text-center text-sm text-light-grey leading-snug`}>
                    {textContent.acf.description}
                </p>
                <div className={`mb-8 xl:hidden lg:hidden sm:flex justify-center cursor-pointer ${expand ? 'rotate-180' : 'rotate-0'}`}>
                    <img onClick={() => setExpand(!expand)} src='/chevron.png' />
                </div>
                <div className="xl:absolute lg:absolute xl:block lg:block sm:hidden right-0" style={{ bottom: -170 }}>
                    <img src='/image_maintenance.png' />
                </div>
            </div>
            <div className="grid grid-cols-2 xl:gap-16 lg:gap-16 sm:gap-0 mb-10">
                <div className="xl:col-span-1 lg:col-span-1 sm:col-span-2 xl:p-0 lg:p-0 sm:p-4">
                    <img src={textContent.better_featured_image.source_url} />
                </div>

                <div className="xl:col-span-1 lg:col-span-1 sm:col-span-2 xl:px-0 lg:px-0 sm:px-4 xl:block lg:block sm:grid">
                    <h2 className="xl:mb-5 lg:mb-5 sm:mb-2 xl:mt-10 lg:mt-10 sm:mt-4 text-dark xl:text-4xl lg:text-4xl sm:text-lg xl:font-normal lg:font-normal sm:font-bold">Features</h2>
                    {
                        feature.map((d) => (
                            <h4 className={`text-light-grey mb-2 border-b-2 border-black 
                            border-opacity-10 xl:text-lg lg:text-lg sm:text-sm xl:leading-normal 
                            lg:leading-normal sm:leading-tight xl:pb-0 lg:pb-0 sm:pb-2  
                            ${!isMobile && 'font-roboto-medium'}`}>
                                {d.title.rendered}
                            </h4>
                        ))
                    }
                </div>
            </div>
            <div className="flex flex-col items-center bg-line-grey relative xl:pb-0 lg:pb-0 sm:pb-5 xl:px-0 lg:px-0 sm:px-4">
                <div className="absolute xl:block lg:block sm:hidden right-0 bottom-0 z-20 " style={{ width: 256, height: 367 }}>
                    <img src='/abstract-motive.png' />
                </div>
                <h2 className="xl:text-4xl lg:text-4xl sm:text-lg text-dark  my-6 mb-4 xl:font-normal lg:font-normal sm:font-bold">Technology</h2>
                <div className="flex items-center w-full lg:mb-6 xl:mb-10 justify-center" >
                    {
                        technology.map((d) => (
                            <div className="w-24 items-center flex justify-center">
                                <img src={d.better_featured_image.source_url} />
                            </div>
                        ))
                    }

                </div>
            </div>
            <div className="flex items-center justify-center bg-darker-blue lg:py-11 w-full xl:py-20">
                <Link href={'/contact-us'}>
                    <button className="p-4 text-white bg-primary font-bold xl:text-xl lg:text-base sm:text-base xl:my-0 lg:my-0 sm:my-9">Contact Us</button>
                </Link>
            </div>
            <Footer />
        </div>
    )
}

export async function getStaticProps() {
    //Header
    const header = await fetchSingleContent(API_URL + 'maintenance-content?slug=banner')

    const textContent = await fetchSingleContent(API_URL + 'maintenance-content?slug=description')

    const feat = await fetch(API_URL + 'maintenance-feature')
    const feature = await feat.json()
    orderItems(feature)

    const tech = await fetch(API_URL + 'maintenance-technology')
    const technology = await tech.json()
    orderItems(technology)
    return {
        props: {
            header,
            textContent,
            feature,
            technology
        },
        revalidate: 10
    }
}