import Footer from 'component/layout/footer'
import Navbar from 'component/layout/navbar'
import { Carousel } from 'antd';
import Link from 'next/link'
import { useEffect, useState } from 'react'
import Head from 'next/head'
import { fetchSingleContent } from 'lib/fetch';
import { API_URL } from 'lib/config';
import { orderItems } from 'lib/order';
import { NextSeo } from 'next-seo'

export default function Hsse(
    {
        header,
        incidentFeature,
        environmentFeature,
        certificationFeature,
        incidentHeader,
        certificationHeader,
        environmentHeader,
        incidentTechnology,
        certificationTechnology,
        environmentTechnology
    }) {
    const [isMobile, setIsMobile] = useState(false)
    const [bigScreen, setBigScreen] = useState(false)

    useEffect(() => {
        if (window.innerWidth > 1300) {
            setBigScreen(true)
        }
        else if (window.innerWidth < 500) {
            setIsMobile(true)
        }
    }, [])
    return (
        <div>
            <Head>
                {/* <NextSeo title="Solution - HSSE" /> */}
                <title>HSSE Software - Rekadia</title>
                <link rel="icon" href="/logo-rekadia.png" />
            </Head>
            <Navbar />
            <div style={{
                backgroundImage: `url(${isMobile ? '/header-hsse-mobile.png' : '/header-hsse.jpg'})`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
            }
            }
                className="relative w-full xl:block lg:block sm:block xl:h-500 lg:h-500 sm:h-275"
            >
                <div className="xl:absolute lg:absolute sm:static xl:p-0 lg:p-0 sm:p-4 left-7 bottom-16 xl:pt-0 lg:pt-0 sm:pt-16">
                    <h1 className="xl:text-white lg:text-white sm:text-line-grey m-0  font-slab xl:text-header lg:text-header sm:text-2xl">
                        {header.content.rendered.replace(/<\/?[^>]+>/gi, '')}
                    </h1>
                    <h2 className="text-white lg:text-sm xl:text-base font-sans leading-snug xl:w-571 lg:w-571 sm:w-auto xl:mt-0 lg:mt-0 sm:mt-4">
                        {header.acf.description}
                    </h2>
                </div>
            </div>
            <div className="grid grid-cols-2 xl:pl-7 lg:pl-7 sm:pl-4 xl:gap-15 lg:gap-15 sm:gap-0 xl:mt-20 lg:mt-20 sm:mt-4 relative">
                <div className="xl:col-span-1 lg:col-span-1 sm:col-span-2 pr-4">
                    <h2 className="mb-4 text-dark text-lg font-bold text-center xl:hidden lg:hidden sm:block">{incidentHeader.title.rendered}</h2>
                    <Carousel autoplay={true} style={{ color: "blue" }}>
                        {
                            incidentFeature.map((d) => (
                                !d.better_featured_image ? null :
                                    <div>
                                        <div className='flex justify-center items-center'>
                                            <img src={d.better_featured_image.source_url} />
                                        </div>
                                    </div>
                            ))
                        }

                    </Carousel>
                </div>
                <div className="xl:col-span-1 lg:col-span-1 sm:col-span-2">
                    <h2 className="mb-5 mt-10 text-dark text-4xl xl:block lg:block sm:hidden">{incidentHeader.title.rendered}</h2>
                    <h2 className="xl:hidden lg:hidden sm:block xl:mb-5 lg:mb-5 sm:mb-2 xl:mt-10 lg:mt-10 sm:mt-4 text-dark xl:text-4xl lg:text-4xl sm:text-lg xl:font-normal lg:font-normal sm:font-bold">Features</h2>
                    {
                        incidentFeature.map((d) => (

                            <h4 className={`text-light-grey mb-2 border-b-2 border-black border-opacity-10 
                            xl:text-lg lg:text-lg sm:text-sm xl:leading-normal lg:leading-normal sm:leading-tight 
                            xl:pb-0 lg:pb-0 sm:pb-2 pr-4  ${!isMobile && 'font-roboto-medium'}`}>
                                {d.title.rendered}
                            </h4>
                        ))
                    }
                    <h2 className="font-bold text-dark xl:text-2xl lg:text-2xl sm:text-lg mb-4 xl:text-left lg:text-left sm:text-center xl:pr-0 lg:pr-0 sm:pr-4 xl:mt-28 lg:mt-28 sm:mt-8">Technology</h2>
                    <div className="flex xl:justify-start lg:justify-start sm:justify-center xl:pr-0 lg:pr-0 sm:pr-4 xl:mb-0 lg:mb-0 sm:mb-5">
                        {
                            incidentTechnology.map((d) => (

                                <div className="w-24 flex  xl:justify-start lg:justify-start sm:justify-center">
                                    <img src={d.better_featured_image.source_url} className="xl:ml-8 lg:ml-8 sm:ml-0" />
                                </div>
                            ))
                        }
                    </div>

                </div>
                <div className="absolute xl:block lg:block sm:hidden  right-0 z-0 top-72 w-72">
                    <img src='/abstract-motive.png' />
                </div>
            </div>
            <div className="grid grid-cols-2 xl:px-12 lg:px-12 sm:px-4 xl:pr-12 lg:pr-12 sm:pr-0  pb-6 xl:pt-9 lg:pt-9 sm:pt-0 xl:gap-7 lg:gap-7 sm:gap-0 xl:mt-20 lg:mt-20 sm:mt-4 bg-line-grey ">

                <div className="xl:col-span-1 lg:col-span-1 sm:col-span-2  xl:block lg:block sm:block xl:order-first lg:order-first sm:order-last">
                    <h2 className="my-5 text-dark text-4xl xl:block lg:block sm:hidden ">{environmentHeader.title.rendered}</h2>
                    <h2 className="xl:hidden lg:hidden sm:block xl:mb-5 lg:mb-5 sm:mb-2 xl:mt-10 lg:mt-10 sm:mt-4 text-dark xl:text-4xl lg:text-4xl sm:text-lg xl:font-normal lg:font-normal sm:font-bold">Features</h2>
                    {
                        environmentFeature.map((d) => (
                            <h4 className={`text-light-grey mb-2 border-b-2 border-black 
                    border-opacity-10 xl:text-lg lg:text-lg sm:text-sm xl:leading-normal 
                    lg:leading-normal sm:leading-tight xl:pb-0 lg:pb-0 sm:pb-2 pr-4 
                     ${!isMobile && 'font-roboto-medium'}`}>
                                {d.title.rendered}
                            </h4>
                        ))
                    }
                    <h2 className="font-bold text-dark xl:text-2xl lg:text-2xl sm:text-lg mb-4 xl:text-left lg:text-left sm:text-center xl:pr-0 lg:pr-0 sm:pr-4 xl:mt-28 lg:mt-28 sm:mt-8">Technology</h2>
                    <div className=" flex items-center xl:pr-0 lg:pr-0 sm:pr-4">
                        {
                            environmentTechnology.map((d) => (

                                <div className="w-24 flex justify-center">
                                    <img src={d.better_featured_image.source_url} />
                                </div>
                            ))
                        }

                    </div>
                </div>
                <div className="xl:col-span-1 lg:col-span-1 sm:col-span-2 pr-4 xl:order-last lg:order-last sm:order-first">
                    <h2 className="my-4 text-dark text-lg font-bold text-center xl:hidden lg:hidden sm:block">{environmentHeader.title.rendered}</h2>

                    <Carousel autoplay={true} style={{ color: "blue" }}>
                        {
                            environmentFeature.map((d) => !d.better_featured_image ? null :
                                <div>
                                    <div className='flex justify-center items-center'>
                                        <img src={d.better_featured_image.source_url} />
                                    </div>
                                </div>
                            )
                        }

                    </Carousel>
                </div>
            </div>
            <div className="grid grid-cols-2 xl:pl-7 lg:pl-7 sm:pl-4 xl:gap-15 lg:gap-15 sm:gap-0 xl:my-20 lg:my-20 sm:my-4">
                <div className="xl:col-span-1 lg:col-span-1 sm:col-span-2 pr-4">
                    <h2 className="mb-4 text-dark text-lg font-bold text-center xl:hidden lg:hidden sm:block">{certificationHeader.title.rendered}</h2>
                    <Carousel autoplay={true} style={{ color: "blue" }}>
                        {
                            certificationFeature.map((d) => !d.better_featured_image ? null :
                                <div>
                                    <div className='flex justify-center items-center'>
                                        <img src={d.better_featured_image.source_url} />
                                    </div>
                                </div>
                            )
                        }

                    </Carousel>
                </div>

                <div className="xl:col-span-1 lg:col-span-1 sm:col-span-2">
                    <h2 className="mb-5 mt-10 text-dark text-4xl xl:block lg:block sm:hidden">{certificationHeader.title.rendered}</h2>
                    <h2 className="xl:hidden lg:hidden sm:block xl:mb-5 lg:mb-5 sm:mb-2 xl:mt-10 lg:mt-10 sm:mt-4 text-dark xl:text-4xl lg:text-4xl sm:text-lg xl:font-normal lg:font-normal sm:font-bold">Features</h2>
                    {
                        certificationFeature.map((d) => (
                            <h4 className={`text-light-grey mb-2 border-b-2 border-black 
                    border-opacity-10 xl:text-lg lg:text-lg sm:text-sm xl:leading-normal 
                    lg:leading-normal sm:leading-tight xl:pb-0 lg:pb-0 sm:pb-2 pr-4 
                    ${!isMobile && 'font-roboto-medium'}`}>
                                {d.title.rendered}
                            </h4>
                        ))
                    }
                    <h2 className="font-bold text-dark xl:text-2xl lg:text-2xl sm:text-lg mb-4 xl:text-left lg:text-left sm:text-center xl:pr-0 lg:pr-0 sm:pr-4 xl:mt-28 lg:mt-28 sm:mt-8">Technology</h2>
                    <div className="flex xl:justify-start lg:justify-start sm:justify-center xl:pr-0 lg:pr-0 sm:pr-4 xl:mb-0 lg:mb-0 sm:mb-5">
                        {
                            certificationTechnology.map((d) => (

                                <div className="w-24 flex  xl:justify-start lg:justify-start sm:justify-center">
                                    <img src={d.better_featured_image.source_url} className="xl:ml-8 lg:ml-8 sm:ml-0" />
                                </div>
                            ))
                        }
                    </div>

                </div>
            </div>
            <div className="flex items-center justify-center bg-darker-blue lg:py-11 w-full xl:py-20 relative sm:py-9">
                <div className="absolute xl:block lg:block sm:hidden right-0 z-0 bottom-0 w-72">
                    <img src='/abstract-motive.png' />
                </div>
                <Link href={'/contact-us'}>
                    <button className="lg:px-8 lg:py-4 xl:px-12 xl:py-4 sm:px-4 sm:py-4 text-white bg-primary font-bold xl:text-xl lg:text-base sm:text-base">Contact Us</button>
                </Link>
            </div>
            <Footer />
        </div>
    )
}

export async function getStaticProps() {
    //Header
    const header = await fetchSingleContent(API_URL + 'hsse-content?slug=header')
    const incidentHeader = await fetchSingleContent(API_URL + 'hsse-content?slug=incident-reporting-software')
    const environmentHeader = await fetchSingleContent(API_URL + 'hsse-content?slug=environment-monitoring-software')
    const certificationHeader = await fetchSingleContent(API_URL + 'hsse-content?slug=online-hsse-certification')

    const incidentFeat = await fetch(API_URL + 'incident-reporting-software')
    const incidentFeature = await incidentFeat.json()
    orderItems(incidentFeature)
    const incidentTech = await fetch(API_URL + 'incident-reporting-software-technology')
    const incidentTechnology = await incidentTech.json()
    orderItems(incidentTechnology)

    const environmentFeat = await fetch(API_URL + 'environment-monitoring-software')
    const environmentFeature = await environmentFeat.json()
    orderItems(environmentFeature)
    const environmentTech = await fetch(API_URL + 'environment-monitoring-software-technology')
    const environmentTechnology = await environmentTech.json()
    orderItems(environmentTechnology)

    const certificationFeat = await fetch(API_URL + 'online-hsse-certification')
    const certificationFeature = await certificationFeat.json()
    orderItems(certificationFeature)
    const certificationTech = await fetch(API_URL + 'online-hsse-certification-technology')
    const certificationTechnology = await certificationTech.json()
    orderItems(certificationTechnology)





    return {
        props: {
            header,
            incidentHeader,
            environmentHeader,
            certificationHeader,
            incidentFeature,
            environmentFeature,
            certificationFeature,
            incidentTechnology,
            environmentTechnology,
            certificationTechnology
        },
        revalidate: 10
    }
}