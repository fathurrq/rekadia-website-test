import Footer from 'component/layout/footer'
import Navbar from 'component/layout/navbar'
import Link from 'next/link'
import { Carousel } from 'antd';
import { useEffect, useState } from 'react'
import Head from 'next/head'
import { fetchSingleContent } from 'lib/fetch';
import { API_URL } from 'lib/config';
import { orderItems } from 'lib/order';
import { NextSeo } from 'next-seo'

export default function ProductMonitoring({ header, feature, technology }) {
    const [isMobile, setIsMobile] = useState(false)
    const [bigScreen, setBigScreen] = useState(false)

    useEffect(() => {
        if (window.innerWidth > 1300) {
            setBigScreen(true)
        }
        else if (window.innerWidth < 500) {
            setIsMobile(true)
        }
    }, [])
    return (
        <div>
            <Head>
                {/* <NextSeo title="Solution - Production Monitoring" /> */}
                <title>Production Monitoring Software - Rekadia</title>
                <link rel="icon" href="/logo-rekadia.png" />

            </Head>
            <Navbar />
            <div style={{
                backgroundImage: `url(${isMobile ? '/header-prod-mobile.png' : '/header-prod-mon.jpg'})`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
            }
            }
                className="relative block w-full xl:h-500 lg:h-500 sm:h-275"
            >
                <div className="xl:absolute lg:absolute sm:static xl:p-0 lg:p-0 sm:p-4 xl:pt-0 lg:pt-0 sm:pt-9 left-7 bottom-7">
                    <h1 className="text-white space-x-2 space-y-8 m-0 font-slab mb-1 xl:text-header lg:text-header sm:text-2xl">
                        {header.content.rendered.replace(/<\/?[^>]+>/gi, '')}
                    </h1>
                    <h2 className="text-white lg:text-sm xl:text-md font-sans leading-snug xl:w-500 lg:w-500 sm:w-auto xl:block lg:block sm:hidden" >
                        {header.acf.description}
                    </h2>
                </div>
            </div>
            <h2 className="text-light-grey text-center text-sm p-4 pt-8 mb-0 font-sans leading-snug xl:w-500 lg:w-500 sm:w-auto xl:hidden lg:hidden sm:block" >
                {header.acf.description}
            </h2>
            <div className="grid grid-cols-2 xl:pl-7 lg:pl-7 sm:pl-0 xl:m-0 lg:m-0 sm:m-4 sm:mr-0 xl:gap-7 lg:gap-7 sm:gap-0">
                <div className="xl:col-span-1 lg:col-span-1 sm:col-span-2 xl:mr-0 lg:mr-0 sm:mr-4">
                    <Carousel autoplay={true} dots={true}>
                        {
                            feature.map((d) => (
                                !d.better_featured_image ? null :
                                    <div>
                                        <div className='flex items-center justify-center'>
                                            <img src={d.better_featured_image.source_url} />
                                        </div>
                                    </div>

                            ))
                        }
                    </Carousel>
                </div>
                <div className="relative flex flex-col justify-center xl:col-span-1 lg:col-span-1 sm:col-span-2">
                    <div className="absolute xl:block lg:block sm:hidden right-0 z-0 lg:top-64 xl:top-56" style={{ width: 256, height: 367 }}>
                        <img src='/abstract-motive.png' />
                    </div>
                    <h2 className="xl:mt-5 lg:mt-5 sm:mt-4 xl:mb-5 lg:mb-5 sm:mb-2 text-dark xl:font-normal lg:font-normal sm:font-bold xl:text-4xl lg:text-4xl sm:text-lg">Features</h2>
                    {
                        feature.map((d) => (
                            <h4 className={`text-light-grey mb-2 border-b-2 border-black border-opacity-10 xl:text-lg lg:text-lg sm:text-sm xl:leading-normal lg:leading-normal sm:leading-tight xl:pb-0 lg:pb-0 sm:pb-2  ${!isMobile && 'font-roboto-medium'}`}>{d.title.rendered}</h4>
                        ))
                    }
                    <p className="font-bold text-dark xl:mt-8 lg:mt-8 sm:mt-6 xl:text-2xl lg:text-2xl sm:text-lg mb-4 xl:pr-0 lg:pr-0 sm:pr-4 sm:text-center xl:text-left lg:text-left">Technology</p>
                    <div className=" flex items-center  xl:pr-0 lg:pr-0 sm:pr-4 xl:mb-0 lg:mb-0 sm:mb-5 ">
                        {
                            technology.map((d) => (
                                <div className="flex w-24 justify-center items-center">
                                    <img src={d.better_featured_image.source_url} />
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>

            <div className="flex items-center justify-center bg-darker-blue lg:py-11 w-full xl:py-20  xl:mt-8 lg:mt-8 sm:mt-0">
                <Link href={'/contact-us'}>
                    <button className="p-4 text-white bg-primary font-bold xl:text-xl text-base xl:my-0 lg:my-0 sm:my-9">Contact Us</button>
                </Link>
            </div>
            <Footer />
        </div>
    )
}

export async function getStaticProps() {
    //Header
    const header = await fetchSingleContent(API_URL + 'prod-monitoring?slug=header')

    //Feature
    const feat = await fetch(API_URL + 'prod-monitoring-feature')
    const feature = await feat.json()
    orderItems(feature)
    const tech = await fetch(API_URL + 'prod-monitoring-technology')
    const technology = await tech.json()
    orderItems(technology)
    return {
        props: {
            header,
            feature,
            technology
        },
        revalidate: 10
    }
}