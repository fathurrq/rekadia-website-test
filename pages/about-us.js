
import Footer from 'component/layout/footer'
import Navbar from 'component/layout/navbar'
import Link from 'next/link'
import { useState, useEffect } from 'react'
import Head from 'next/head'
import { API_URL } from 'lib/config'
import { orderItems } from 'lib/order'
import { fetchSingleContent } from 'lib/fetch'

export default function AboutUs({ posts, header, ourSpeciality, ourMission, ourSpecialityMobile, bottomContent, ourValue }) {
    const [expand, setExpand] = useState(false)
    const [isMobile, setIsMobile] = useState(false)
    const [bigScreen, setBigScreen] = useState(false)

    useEffect(() => {
        if (window.innerWidth > 1300) {
            setBigScreen(true)
        }
        else if (window.innerWidth < 500) {
            setIsMobile(true)
        }
    }, [])
    return (
        <div>
            <Head>
                {/* <NextSeo title="About Us" /> */}
                <title>About - Rekadia</title>
                <link rel="icon" href="/logo-rekadia.png" />
            </Head>
            <Navbar />
            <div style={{
                backgroundImage: `url(${header.better_featured_image.source_url})`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover",
                height: 600
            }
            }
                className="relative w-full xl:flex lg:flex sm:hidden"
            >
                <div className="absolute left-7 lg:bottom-48">
                    <h1 className="text-white space-x-2 space-y-8 font-slab  p-0 m-0 mb-1 text-header" >
                        {header.content.rendered.replace(/<\/?[^>]+>/gi, '')}
                    </h1>
                    <p className="text-white text-sm lg:w-3/5 xl:w-2/5 leading-snug">
                        {
                            header.acf.description
                        }
                    </p>
                </div>
            </div>
            <div className="grid grid-cols-2 xl:px-7 lg:px-7 sm:px-0 xl:py-4 lg:py-4 sm:py-0">
                <div className="xl:col-span-1 lg:col-span-1 sm:col-span-2">
                    <img src={isMobile ? ourSpecialityMobile.better_featured_image.source_url : ourSpeciality.better_featured_image.source_url} className='sm:w-full lg:w-auto xl:w-auto' />
                </div>
                <div className="flex flex-col justify-center xl:col-span-1 lg:col-span-1 sm:col-span-2 xl:p-0 lg:p-0 sm:p-4">
                    <div>
                        <h2 className="text-dark xl:text-2xl lg:text-2xl sm:text-lg xl:mb-6 lg:mb-6 sm:mb-2 xl:font-normal lg:font-normal sm:font-bold">{ourSpeciality.title.rendered}</h2>
                        <p className=" text-light-grey text-xs w-472 sm:w-auto">{
                            ourSpeciality.content.rendered.replace(/<\/?[^>]+>/gi, '')
                        }</p>
                    </div>
                    <div>
                        <p className={` mb-0 text-light-grey text-xs w-472 sm:w-auto xl:block lg:block sm:${expand ? 'block' : 'hidden'}`} >
                            {ourSpeciality.acf.description}</p>
                    </div>
                    <div className={`xl:hidden lg:hidden sm:flex justify-center cursor-pointer ${expand ? 'rotate-180' : 'rotate-0'}`}>
                        <img onClick={() => setExpand(!expand)} src='/chevron.png' />
                    </div>
                </div>
            </div>
            <div className="grid grid-cols-2 xl:h-440 lg:h-440 sm:h-auto">
                <div className="gradient-bg flex flex-col items-center justify-center xl:col-span-1 lg:col-span-1 sm:col-span-2 xl:h-auto lg:h-auto sm:h-56">
                    <p className=" text-white text-lg font-bold text-center xl:mb-9 lg:mb-9 sm:mb-6 xl:text-2xl">{ourMission.title.rendered}</p>
                    <p className="xl:px-0 lg:px-0 sm:px-4 text-white xl:text-4xl lg:text-4xl sm:text-2xl text-center m-0 w-500 sm:w-auto xl:font-normal lg:font-normal sm:font-bold sm:leading-tight xl:leading-normal lg:leading-normal">{ourMission.content.rendered.replace(/<\/?[^>]+>/gi, '')}</p>
                </div>
                <div className="bg-light-green flex items-center justify-center xl:p-4 lg:p-4 sm:p-6 xl:h-440 lg:h-440 sm:h-auto xl:col-span-1 lg:col-span-1 sm:col-span-2" >
                    <img src={ourValue.better_featured_image.source_url} />
                </div>
            </div>
            <div className='xl:mb-12 lg:mb-12 sm:mb-2'>

                <h2 className="xl:font-normal lg:font-normal sm:font-bold text-center xl:mt-12 lg:mt-12 sm:mt-3 xl:text-4xl lg:text-4xl sm:text-lg text-dark xl:mb-10 lg:mb-10 sm:mb-2">Why Work With Us</h2>
                {
                    posts.map((d, i) => {
                        if (i % 2 == 0) return (
                            <div className="grid grid-cols-2 xl:px-7 lg:px-7 sm:px-4">
                                <div className="flex justify-center items-center xl:col-span-1 lg:col-span-1 sm:col-span-2">
                                    <img src={d.better_featured_image.source_url} />

                                </div>
                                <div className="flex items-center justify-center xl:col-span-1 lg:col-span-1 sm:col-span-2">
                                    <p className="sm:text-xs lg:text-sm xl:text-sm text-light-grey leading-relaxed xl:mx-15 lg:mx-15 sm:mx-0 px-0 mb-0 xl:my-0 lg:my-0 sm:my-4">
                                        {d.content.rendered.replace(/<\/?[^>]+>/gi, '')}
                                    </p>
                                </div>
                            </div>
                        )
                        else return (
                            <div className="grid grid-cols-2  xl:px-7 lg:px-7 sm:px-4">
                                <div className="flex items-center xl:col-span-1 lg:col-span-1 sm:col-span-2 xl:order-first lg:order-first sm:order-last">
                                    <p className="sm:text-xs lg:text-sm xl:text-sm text-light-grey leading-relaxed   xl:mx-15 lg:mx-15 sm:mx-0 px-0 mb-0 xl:my-0 lg:my-0 sm:my-4">
                                        {d.content.rendered.replace(/<\/?[^>]+>/gi, '')}
                                    </p>
                                </div>
                                <div className="flex justify-center items-center xl:col-span-1 lg:col-span-1 sm:col-span-2">
                                    <img src={d.better_featured_image.source_url} />
                                </div>
                            </div>
                        )
                    }
                    )
                }
            </div>
            <div className="w-full xl:h-64 lg:h-64 sm:h-auto xl:py-0 lg:py-0 sm:py-4 bg-darker-blue flex flex-col items-center justify-center">
                <p className=" text-white font-slab text-center leading-tight mb-6 xl:max-w-md lg:max-w-md sm:w-4/5 xl:text-header lg:text-header sm:text-2xl">{bottomContent?.acf?.description}</p>
                <Link href={'/contact-us'}>
                    <button className="xl:px-12 lg:px-12 sm:px-6 xl:py-3 lg:py-3 sm:py-1 text-white bg-primary font-bold xl:text-base lg:text-base sm:text-lg">Contact Us</button>
                </Link>

            </div>

            <Footer />
        </div>
    )
}
export async function getStaticProps() {
    //Client logo
    const res = await fetch(API_URL + 'why-work-with-us')
    const posts = await res.json()
    orderItems(posts)

    //About Us content 
    const header = await fetchSingleContent(API_URL + 'about-us-content?slug=header')
    const ourSpeciality = await fetchSingleContent(API_URL + 'about-us-content?slug=our-speciality')
    const ourSpecialityMobile = await fetchSingleContent(API_URL + 'about-us-content?slug=our-speciality-mobile-image')
    const ourMission = await fetchSingleContent(API_URL + 'about-us-content?slug=our-mission')
    const ourValue = await fetchSingleContent(API_URL + 'about-us-content?slug=our-value')
    const bottomContent = await fetchSingleContent(API_URL + 'about-us-content?slug=bottom-content')
    return {
        props: {
            posts,
            header,
            ourSpeciality,
            ourSpecialityMobile,
            ourMission,
            bottomContent,
            ourValue
        },
        revalidate: 10
        
    }
}