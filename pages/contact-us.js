import Footer from 'component/layout/footer'
import Navbar from 'component/layout/navbar'
import { useState, useEffect } from 'react'
import Head from 'next/head'
import { fetchSingleContent } from 'lib/fetch'
import { API_URL } from 'lib/config'
import axios from 'axios'
import { Modal } from 'antd'

export default function ContactUs({ headQuarter, workshop, email, phone, whatsapp }) {
    const [name, setName] = useState('')
    const [emailForm, setEmailForm] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')
    const [subject, setSubject] = useState('')
    const [message, setMessage] = useState('')
    const [isMobile, setIsMobile] = useState(false)
    const [bigScreen, setBigScreen] = useState(false)
    // const [modal, setModal] = useState(false)
    useEffect(() => {
        if (window.innerWidth > 1300) {
            setBigScreen(true)
        }
        else if (window.innerWidth < 500) {
            setIsMobile(true)
        }
    }, [])
    const save = async () => {
        let bodyFormData = new FormData()
        bodyFormData.append('name', name)
        bodyFormData.append('email', emailForm)
        bodyFormData.append('phone', phoneNumber)
        bodyFormData.append('subject', subject)
        bodyFormData.append('message', message)

        await axios({
            headers: { 'Content-Type': 'multipart/form-data' },
            method: 'post',
            url: `https://cms.rekadia.co.id/wp-json/contact/v1/send`,
            data: bodyFormData
        })
            .then((e) => {
                // alert(`Email terkirim ke ${emailForm}`)
                setEmailForm('')
                setName('')
                setPhoneNumber('')
                setSubject('')
                setMessage('')
                Modal.success({
                    content: 'Thank you for contacting us. Our representative will be in touch soon.',
                });
            })
            .catch((e) => {
                Modal.error({
                    content: 'Email Not Send',

                })
            })
    }
    return (
        <div>
            <Head>
                {/* <NextSeo title="Contact Us" /> */}
                <title>Contact Us - Rekadia</title>
                <link rel="icon" href="/logo-rekadia.png" />
            </Head>
            <Navbar />
            <div className="relative">
                <div style={{
                    backgroundImage: `url(${isMobile ? '/header-contact-mobile.jpg' : '/header-contact.jpg'})`,
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "cover"
                }
                }
                    className="relative w-full flex xl:h-900 lg:h-900 sm:h-534"

                >
                </div>
                <div className="absolute xl:flex lg:flex sm:flex xl:top-48 lg:top-48 sm:top-16 flex-col w-full items-center px-4">
                    <h2 className=" text-white mb-3 font-slab xl:text-header lg:text-header sm:text-2xl sm:text-center lg:text-center xl:text-center">Feel Free to Contact Us</h2>
                    <input value={name} onChange={(e) => setName(e.target.value)} className="rounded p-3 pl-2 shadow-inner mb-3 xl:mt-0 xl:w-150 lg:w-150 sm:w-full xl:h-10 lg:h-10 sm:h-8" style={{ boxShadow: 'inset 0px 1px 3px rgba(0, 0, 0, 0.5)' }} placeholder="Name*" />
                    <input value={emailForm} onChange={(e) => setEmailForm(e.target.value)} className="rounded p-3 pl-2 shadow-inner mb-3 xl:mt-0 xl:w-150 lg:w-150 sm:w-full xl:h-10 lg:h-10 sm:h-8" style={{ boxShadow: 'inset 0px 1px 3px rgba(0, 0, 0, 0.5)' }} placeholder="Email*" />
                    <input value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)} className="rounded p-3 pl-2 shadow-inner mb-3 xl:mt-0 xl:w-150 lg:w-150 sm:w-full xl:h-10 lg:h-10 sm:h-8" style={{ boxShadow: 'inset 0px 1px 3px rgba(0, 0, 0, 0.5)' }} placeholder="Phone Number" />
                    <input value={subject} onChange={(e) => setSubject(e.target.value)} className="rounded p-3 pl-2 shadow-inner mb-3 xl:mt-0 xl:w-150 lg:w-150 sm:w-full xl:h-10 lg:h-10 sm:h-8" style={{ boxShadow: 'inset 0px 1px 3px rgba(0, 0, 0, 0.5)' }} placeholder="Subject*" />
                    <input value={message} onChange={(e) => setMessage(e.target.value)} className="rounded pl-2 shadow-inner mb-3 xl:mt-0 xl:w-150 lg:w-150 sm:w-full xl:h-60 lg:h-60 sm:h-24 xl:pb-51 lg:pb-51 sm:pb-16" style={{ boxShadow: 'inset 0px 1px 3px rgba(0, 0, 0, 0.5)' }} placeholder="Your Message" />
                    <div className="flex justify-between xl:h-auto lg:h-auto sm:h-10 xl:items-start lg:items-start sm:items-center mb-3 xl:mt-0 lg:mt-0 sm:mt-4 xl:w-150 lg:w-150 sm:w-full" >
                        <p className="lg:text-sm xl:text-base text-line-grey m-0 pl-3">•  must be filled</p>
                        <button className="py-3 px-7 text-white bg-primary font-bold xl:text-xl xl:mt-11 lg:mt-11 sm:mt-0" onClick={save}>Send</button>

                    </div>
                    {/* <div className="flex justify-end xl:mt-0 xl:w-150 lg:w-150 sm:w-80" >

                        <button className="p-4 text-white bg-primary font-bold xl:text-xl ">Contact Us</button>
                    </div> */}
                </div>
            </div>
            {/* <div style={{ height: 30 }} className="xl:block lg:block sm:hidden">``

            </div> */}
            <div className="w-full bg-darker-blue grid grid-cols-2">
                <div className="xl:p-5 lg:p-5 sm:p-4 flex xl:col-span-1 lg:col-span-1 sm:col-span-2">
                    <div className="w-full xl:pl-3 lg:pl-3 sm:pl-0">
                        <img src={headQuarter.better_featured_image.source_url} />

                    </div>
                </div>
                <div className="grid grid-cols-2 xl:py-4 lg:py-4 sm:py-6  xl:col-span-1 lg:col-span-1 sm:col-span-2 xl:px-0 lg:px-0 sm:px-4">
                    <div className=" xl:col-span-1 lg:col-span-1 sm:col-span-2 xl:mb-0 lg:mb-0 sm:mb-4">
                        <h2 className="text-2xl text-line-grey xl:mb-10 lg:mb-4">Location</h2>
                        <div className="flex pt-0 items-center xl:mb-0 lg:mb-0 sm:mb-0">
                            <div className="flex items-center mr-4 h-8 w-8">
                                <img src='/pin-white.svg' />
                            </div>
                            <h2 className="text-line-grey font-bold xl:text-lg lg:text-lg sm:text-base m-0">{headQuarter.content.rendered.replace(/<\/?[^>]+>/gi, '')}</h2>
                        </div>
                        <div className="flex flex-col xl:mb-3 lg:mb-0 ml-12">
                            <h2 className="text-line-grey text-sm pr-10">{headQuarter.acf.description}</h2>
                        </div>
                        <div className="flex pt-0 items-end mb-0">
                            <div className="flex items-center mr-4 w-8 h-8">
                                <img src='/pin-white.svg' />

                            </div>
                            <h2 className="text-line-grey font-bold xl:text-lg lg:text-lg sm:text-base m-0">{workshop.content.rendered.replace(/<\/?[^>]+>/gi, '')}</h2>
                        </div>
                        <div className="flex flex-col xl:mb-3 lg:mb-0 ml-12">
                            <h2 className="text-line-grey text-sm w-48 m-0 p-0">
                                {workshop.acf.description}
                            </h2>
                        </div>
                    </div>
                    <div className="xl:border-l lg:border-l xl:border-solid lg:border-solid sm:border-none xl:pl-5 lg:pl-5 sm:pl-0  xl:col-span-1 lg:col-span-1 sm:col-span-2">
                        <h2 className="text-2xl text-line-grey xl:mb-10 lg:mb-4 sm:mb-2">Contact</h2>
                        <div className="flex mb-6 items-center">
                            <div className="xl:mr-0 lg:mr-0 sm:mr-4 flex items-center justify-center w-8 h-8">
                                <img src='/wa-white.png' />
                            </div>
                            <h2 className="text-line-grey text-sm xl:ml-4 lg:ml-4 sm:ml-0 mb-0" >{phone.acf.description}</h2>
                        </div>
                        <div className="flex mb-6 items-center">
                            <div className="xl:mr-0 lg:mr-0 sm:mr-4 flex items-center justify-center w-8 h-8">
                                <img src='/whatsapp-white.png' />
                            </div>
                            <a target={"_blank"} className="text-line-grey text-sm xl:ml-4 lg:ml-4 sm:ml-0 mb-0" href={`https://api.whatsapp.com/send?phone=${whatsapp?.acf?.description || ''}`}>+{whatsapp.acf.description}</a>
                        </div>
                        <div className="flex mb-4 items-center">
                            <div className="xl:mr-0 lg:mr-0 sm:mr-4 flex items-center justify-center w-8 h-8">
                                <img src='/mail-white.png' />
                            </div>
                            <a href={`mailto:${email?.acf?.description || ''}`} className="text-line-grey text-sm xl:ml-4 lg:ml-4 sm:ml-0 mb-0">{email.acf.description}</a>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}
export async function getStaticProps() {
    //Contact Us content 
    const headQuarter = await fetchSingleContent(API_URL + 'about-rekadia?slug=head-quarter')
    const workshop = await fetchSingleContent(API_URL + 'about-rekadia?slug=workshop')
    const whatsapp = await fetchSingleContent(API_URL + 'about-rekadia?slug=whatsapp')
    const email = await fetchSingleContent(API_URL + 'about-rekadia?slug=email')
    const phone = await fetchSingleContent(API_URL + 'about-rekadia?slug=phone')

    return {
        props: {
            headQuarter,
            workshop,
            email,
            phone,
            whatsapp
        },
        revalidate: 10
    }
}